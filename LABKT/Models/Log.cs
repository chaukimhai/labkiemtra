﻿

using System.ComponentModel.DataAnnotations;

namespace LABKT.Models
{
    public class Log
    {
        [Key]
        public int LogsID { get; set; }
        public int TransactionalID { get; set; }
        public DateTime LoginDate { get; set; }
        public TimeSpan LoginTime { get; set; }
        public Transaction? Transaction { get; set; }
    }
}
